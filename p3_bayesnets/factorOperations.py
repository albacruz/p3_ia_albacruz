# factorOperations.py
# -------------------
# Licensing Information:  You are free to use or extend these projects for
# educational purposes provided that (1) you do not distribute or publish
# solutions, (2) you retain this notice, and (3) you provide clear
# attribution to UC Berkeley, including a link to http://ai.berkeley.edu.
# 
# Attribution Information: The Pacman AI projects were developed at UC Berkeley.
# The core projects and autograders were primarily created by John DeNero
# (denero@cs.berkeley.edu) and Dan Klein (klein@cs.berkeley.edu).
# Student side autograding was added by Brad Miller, Nick Hay, and
# Pieter Abbeel (pabbeel@cs.berkeley.edu).


from bayesNet import Factor
import operator as op
import util
import functools

def joinFactorsByVariableWithCallTracking(callTrackingList=None):


    def joinFactorsByVariable(factors, joinVariable):
        """
        Input factors is a list of factors.
        Input joinVariable is the variable to join on.

        This function performs a check that the variable that is being joined on 
        appears as an unconditioned variable in only one of the input factors.

        Then, it calls your joinFactors on all of the factors in factors that 
        contain that variable.

        Returns a tuple of 
        (factors not joined, resulting factor from joinFactors)
        """

        if not (callTrackingList is None):
            callTrackingList.append(('join', joinVariable))

        currentFactorsToJoin =    [factor for factor in factors if joinVariable in factor.variablesSet()]
        currentFactorsNotToJoin = [factor for factor in factors if joinVariable not in factor.variablesSet()]

        # typecheck portion
        numVariableOnLeft = len([factor for factor in currentFactorsToJoin if joinVariable in factor.unconditionedVariables()])
        if numVariableOnLeft > 1:
            print("Factor failed joinFactorsByVariable typecheck: ", factor)
            raise ValueError("The joinBy variable can only appear in one factor as an \nunconditioned variable. \n" +  
                               "joinVariable: " + str(joinVariable) + "\n" +
                               ", ".join(map(str, [factor.unconditionedVariables() for factor in currentFactorsToJoin])))
        
        joinedFactor = joinFactors(currentFactorsToJoin)
        return currentFactorsNotToJoin, joinedFactor

    return joinFactorsByVariable

joinFactorsByVariable = joinFactorsByVariableWithCallTracking()

#-------------------------------------------------------------------------
# Ejercicio 3: Recibe una lista de factores y devuelve un factor
# cuyas probabilidades son el producto de las probabilidades entrantes
#-------------------------------------------------------------------------

def joinFactors(factors):
    """
    Question 3: Your join implementation 

    Input factors is a list of factors.  
    
    You should calculate the set of unconditioned variables and conditioned 
    variables for the join of those factors.

    Return a new factor that has those variables and whose probability entries 
    are product of the corresponding rows of the input factors.

    You may assume that the variableDomainsDict for all the input 
    factors are the same, since they come from the same BayesNet.

    joinFactors will only allow unconditionedVariables to appear in 
    one input factor (so their join is well defined).

    Hint: Factor methods that take an assignmentDict as input 
    (such as getProbability and setProbability) can handle 
    assignmentDicts that assign more variables than are in that factor.

    Useful functions:
    Factor.getAllPossibleAssignmentDicts
    Factor.getProbability
    Factor.setProbability
    Factor.unconditionedVariables
    Factor.conditionedVariables
    Factor.variableDomainsDict
    """

    # typecheck portion
    setsOfUnconditioned = [set(factor.unconditionedVariables()) for factor in factors]
    if len(factors) > 1:
        intersect = functools.reduce(lambda x, y: x & y, setsOfUnconditioned)
        if len(intersect) > 0:
            print("Factor failed joinFactors typecheck: ", factor)
            raise ValueError("unconditionedVariables can only appear in one factor. \n"
                    + "unconditionedVariables: " + str(intersect) + 
                    "\nappear in more than one input factor.\n" + 
                    "Input factors: \n" +
                    "\n".join(map(str, factors)))


    "*** YOUR CODE HERE ***"
    # ejemplos de uso:
    # joinFactors(P(X|Y), P(Y)) = P(X,Y)
    # joinFactors(P(V,W|X,Y,Z), P(X,Y|Z)) = p(V,W, X,Y|Z)
    # joinFactors(P(X|Y,Z), P(Y)) = P(X,Y|Z)
    # joinFactors(P(V|W), P(X|Y), P(Z)) = P(V,X,Z|W,Y)

    # lista de variables condicionadas y no condicionadas
    uncondicionedSet = set()
    condicionedSet = set()

    # se añaden los factores a una lista
    factors = list(factors)

    # se añaden las variables condicionadas y no condicionadas a sus conjuntos
    for factor in factors:
        condicionedSet = condicionedSet.union(factor.conditionedVariables())
        uncondicionedSet = uncondicionedSet.union(factor.unconditionedVariables())
    
    # se eliminan las variables condicionadas de las no condicionadas
    for uncondicioned in uncondicionedSet:
        if uncondicioned in condicionedSet:
            condicionedSet.remove(uncondicioned)
    
    # se crea el nuevo factor con las variables condicionadas y no condicionadas
    joined_domain = factors[0].variableDomainsDict()
    joinedFactor = Factor(uncondicionedSet, condicionedSet, joined_domain)
    assignments = joinedFactor.getAllPossibleAssignmentDicts()

    # se calculan las probabilidades del nuevo factor dadas las probabilidades
    # de los factores de entrada
    for assignment in assignments:
        joinedFactor.setProbability(assignment, 1)
        
        for factor in factors:
            probFactor = joinedFactor.getProbability(assignment)
            probF = factor.getProbability(assignment)
            
            joinedFactor.setProbability(assignment, probFactor * probF)

    return joinedFactor
    "*** END YOUR CODE HERE ***"


def eliminateWithCallTracking(callTrackingList=None):

    #-----------------------------------------------------------
    # Ejercicio 4: Elimina una variable de un factor
    #-----------------------------------------------------------
    
    def eliminate(factor, eliminationVariable):
        """
        Question 4: Your eliminate implementation 

        Input factor is a single factor.
        Input eliminationVariable is the variable to eliminate from factor.
        eliminationVariable must be an unconditioned variable in factor.
        
        You should calculate the set of unconditioned variables and conditioned 
        variables for the factor obtained by eliminating the variable
        eliminationVariable.

        Return a new factor where all of the rows mentioning
        eliminationVariable are summed with rows that match
        assignments on the other variables.

        Useful functions:
        Factor.getAllPossibleAssignmentDicts
        Factor.getProbability
        Factor.setProbability
        Factor.unconditionedVariables
        Factor.conditionedVariables
        Factor.variableDomainsDict
        """
        # autograder tracking -- don't remove
        if not (callTrackingList is None):
            callTrackingList.append(('eliminate', eliminationVariable))

        # typecheck portion
        if eliminationVariable not in factor.unconditionedVariables():
            print("Factor failed eliminate typecheck: ", factor)
            raise ValueError("Elimination variable is not an unconditioned variable " \
                            + "in this factor\n" + 
                            "eliminationVariable: " + str(eliminationVariable) + \
                            "\nunconditionedVariables:" + str(factor.unconditionedVariables()))
        
        if len(factor.unconditionedVariables()) == 1:
            print("Factor failed eliminate typecheck: ", factor)
            raise ValueError("Factor has only one unconditioned variable, so you " \
                    + "can't eliminate \nthat variable.\n" + \
                    "eliminationVariable:" + str(eliminationVariable) + "\n" +\
                    "unconditionedVariables: " + str(factor.unconditionedVariables()))

        "*** YOUR CODE HERE ***"
        # Lista de variables condicionadas y no condicionadas
        unconditionedVar = factor.unconditionedVariables()
        conditionedVar = factor.conditionedVariables()

        # dominio del factor
        domain = factor.variableDomainsDict()

        # elimino la incondicionada objetivo
        unconditionedVar.remove(eliminationVariable)

        # creo el nuevo factor
        newFactor = Factor(unconditionedVar, conditionedVar, domain)

        #se asignan las probabilidades
        assignments = newFactor.getAllPossibleAssignmentDicts()

        # para cada asignacion de variables
        for assignment in assignments:
            # Lista de posibles valores de la variable a eliminar.
            eliminateVarValues = factor.variableDomainsDict()[eliminationVariable]

            prob = 0.0

            # para cada valor de la variable a eliminar
            for value in eliminateVarValues:
                # asigno el valor a la variable a eliminar
                assignment[eliminationVariable] = value
                prob_value = factor.getProbability(assignment)
                prob += prob_value
        
            # al nuevo factor creado, le asigno la probabilidad calculada (suma de probabilidades)
            newFactor.setProbability(assignment, prob)
        
        return newFactor
        
                
    "*** END YOUR CODE HERE ***"

    return eliminate

eliminate = eliminateWithCallTracking()

#-------------------------------------------------------------------------------------------
# Ejercicio 5: Normaliza un factor (escala todas las entradas en el Factor para que la suma
# de las entradas en el Factor sea 1.)
#--------------------------------------------------------------------------------------------

def normalize(factor):
    """
    Question 5: Your normalize implementation 

    Input factor is a single factor.

    The set of conditioned variables for the normalized factor consists 
    of the input factor's conditioned variables as well as any of the 
    input factor's unconditioned variables with exactly one entry in their 
    domain.  Since there is only one entry in that variable's domain, we 
    can either assume it was assigned as evidence to have only one variable 
    in its domain, or it only had one entry in its domain to begin with.
    This blurs the distinction between evidence assignments and variables 
    with single value domains, but that is alright since we have to assign 
    variables that only have one value in their domain to that single value.

    Return a new factor where the sum of the all the probabilities in the table is 1.
    This should be a new factor, not a modification of this factor in place.

    If the sum of probabilities in the input factor is 0,
    you should return None.

    This is intended to be used at the end of a probabilistic inference query.
    Because of this, all variables that have more than one element in their 
    domain are assumed to be unconditioned.
    There are more general implementations of normalize, but we will only 
    implement this version.

    Useful functions:
    Factor.getAllPossibleAssignmentDicts
    Factor.getProbability
    Factor.setProbability
    Factor.unconditionedVariables
    Factor.conditionedVariables
    Factor.variableDomainsDict
    """

    # typecheck portion
    variableDomainsDict = factor.variableDomainsDict()
    for conditionedVariable in factor.conditionedVariables():
        if len(variableDomainsDict[conditionedVariable]) > 1:
            print("Factor failed normalize typecheck: ", factor)
            raise ValueError("The factor to be normalized must have only one " + \
                            "assignment of the \n" + "conditional variables, " + \
                            "so that total probability will sum to 1\n" + 
                            str(factor))

    "*** YOUR CODE HERE ***"
    totalProb = 0.0

    # lista con las posibles combinaciones del factor
    assignments = factor.getAllPossibleAssignmentDicts()

    for assignment in assignments:
        # sumar posibilidades del factor original
        probF = factor.getProbability(assignment)
        totalProb += probF

        # determinamos variables condicionadas e incondicionadas del nuevo factor
        unconditionedVar = factor.unconditionedVariables()
        conditionedVar = factor.conditionedVariables()

        # creamos una copia de ambas listas para poder modificarlas
        unconditionedVar_aux = unconditionedVar.copy()
        conditionedVar_aux = conditionedVar.copy()

        # para cada variable incondicionada 
        for var in unconditionedVar:
            values = factor.variableDomainsDict()[var]
            n_values = len(values)

            # si solo tiene un valor, la añadimos a las condicionadas
            if n_values == 1:
                unconditionedVar_aux.remove(var)
                conditionedVar_aux.add(var)

    # creo el nuevo factor en base a las listas de variables condicionadas y no condicionadas
    newFactor = Factor(unconditionedVar_aux, conditionedVar_aux, variableDomainsDict)
    assignments = factor.getAllPossibleAssignmentDicts()
    
    for assignment in assignments:
        # probabilidad de que ocurra esta combinacion (asignacion)
        probability_norm = factor.getProbability(assignment) / totalProb
        # añado la probabilidad al nuevo factor
        newFactor.setProbability(assignment, probability_norm)

    return newFactor
    "*** END YOUR CODE HERE ***"

